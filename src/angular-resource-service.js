(function(window, angular, undefined) {'use strict';
	angular.module('imagus.service.http.resource', ['service.http'])
    .constant('resourceSvcBaseUrl', 'development.imagus.com.au')
	.service('imgResourceService', function(HttpService, HttpOptions, resourceSvcBaseUrl) {
		// Service
		var service = {
			uploadResource: uploadResource
		};
		
		return service;
		
		// Implementation
		
		function _buildUrl(url) {
			return 'https://' + resourceSvcBaseUrl + url;
		}
		
		function uploadResource(fdata, fname, ftype) {
	        var url = '';
	
	        var params = {
	            'file_name': fname
	        };
	
			var options = new HttpOptions({
				transformRequest: [],
				data: fdata,
				headers: {"Content-Type": ftype}
			});

	        return HttpService.handleRequest(HttpService.POST, _buildUrl(url), true, params, options);
	    }
	});
})(window, window.angular);