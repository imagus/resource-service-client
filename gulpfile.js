'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify')
var sourcemaps = require('gulp-sourcemaps');
var path = require('path')
var rename = require('gulp-rename')
var browserify = require('browserify')
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var ngAnnotate = require('gulp-ng-annotate');
var argv = require('yargs').argv;
var replace = require('gulp-replace');
var del = require('del');


if (!argv.baseurl) {
    console.error('Parameter baseurl not provided');
    process.exit(1);
}

var src = './src';
var dest = './bin';
var temp = './temp';
var mainFile = 'angular-resource-service.js';

gulp.task('default', ['configure', 'browserify', 'clean']);

gulp.task('browserify', ['configure'], function() {
    var b = browserify({
        entries: path.join(temp, mainFile),
        debug: true
    });
    
    return b.bundle()
        .pipe(source(mainFile))
        .pipe(buffer())
        .pipe(ngAnnotate())
        .pipe(gulp.dest(dest))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify({mangle: false})) // mangling causes Angular to break
        .pipe(rename({extname: '.min.js'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dest));
});

gulp.task('configure', function() {
    gulp.src([src + '/' + mainFile])
        .pipe(replace('development.imagus.com.au', argv.baseurl))
        .pipe(gulp.dest(temp));
});

gulp.task('clean', ['configure', 'browserify'], function() {
    return del(['temp']);
});
    


